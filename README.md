# Bible Tools #


### Génération des Textes ###

1. Installer Node.js (http://nodejs.org/download/)
2. Se rendre dans le répertoire `textgenerator`
3. Lancer `npm install` pour installer les packages dépendants
4. Créer le fichier `config.js` à partir de `config.js.template` et modifier la valeur de "textsPath" pour indiquer le répertoire contenant les textes du site de la BJC:
	ex : "textsPath": "/var/www/html/bibledejesuschrist.org/content/texts/"
5. Pour générer un texte:
	Lancer `node generate.js -v version -i` (`-v` génèrera la version passée en paramètre et qui correspond à un répertoire dans `input`, `-i` permet de générer les fichiers index pour optimiser les recherches)
	Exemples:
	`node generate.js -v martin -i` (pour générer la bible Martin)
	`node generate_bjc.js -v bjc -i` (pour générer ou mettre à jour la BJC, il faut utiliser `generate_bjc.js` en lieu et place de `generate.js`)
5. S'il s'agit d'une nouvelle bible, lancer `node create_texts_index.js` pour mettre à jour la liste des versions du site.

### Ajout de nouvelles Bibles/nouveaux Textes ###

1. Créer un répertoire dans `textgenerator/input/NouvelleVersion/`
2. Créer un fichier `info.json` file dans ce répertoire avec les infos sur la nouvelle version(d, name, language ...)
3. Ajouter le texte dans le répertoire
4. Depuis le répertoire `textgenerator`, lancer `node generate.js -v <foldername> -i` pour générer la nouvelle version.
5. Lancer `node create_texts_index.js` pour mettre à jour la liste des versions du site.


### Mise à jour BJC ###

1. Mettre à jour le fichier osis avec les dernières corrections en utilisant le script `tex2osis.pl` du dépôt BJC (répertoire scripts)
	`perl tex2osis.pl`
2. Copier le fichier généré (`osis/bjc.xml`) dans le répertoire `textgenerator/input/bjc`
   Il est également possible de récupérer le fichier depuis le dépôt BJC-public avec la commande curl suivante:
   `curl -O https://git.anjc-prod.org/anjc-prod/BJC-Public/raw/master/osis/bjc.xml`
3. Se positionner dans le répertoire textgenerator et lancer la commande suivante:
   `node generate_bjc.js -v bjc -i`
   Cette commande va générer les textes (et les fichiers index) au format inscript dans le répertoire `content/texts/fra_bjc` (défini dans le fichier `config.js` et `info.json` pour fra_bjc).
4. Vérifier sur le site local que tout est ok et que les dernières modifications ont bien été prises en compte
5. Faire un push de la branche master
	`git push origin master && git push ovh master`
6. Passer dans la branche production avec `git checkout production`
7. Faire un merge à partir de la branche master: `git merge master`
8. Faire un git push de la branche production.
	`git push origin production && git push ovh production`
8. Vérifier si le site a bien été mise à jour.
9. Repasser sur la branche master pour la prochaine fois: `git checkout master`.

